import tkinter as tk
import blinds


class BlindsGUI(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.master.title("Blinds GUI")

        # Initialize current round
        self.current_round = 1

        # Create labels to display current blinds and round
        self.blinds_label = tk.Label(self.master, text=f"Small Blind: {blinds.current_small_blind} Big Blind: {blinds.current_big_blind}")
        self.round_label = tk.Label(self.master, text=f"Round: {self.current_round}")
        self.blinds_label.pack()
        self.round_label.pack()

        # Create button to increase blinds and round
        self.button = tk.Button(self.master, text="Next Round", command=self.update_blinds)
        self.button.pack()

    def update_blinds(self):
        # Update blinds and round
        blinds.update_blinds()
        self.current_round += 1

        # Update labels
        self.blinds_label.config(text=f"Small Blind: {blinds.current_small_blind} Big Blind: {blinds.current_big_blind}")
        self.round_label.config(text=f"Round: {self.current_round}")


if __name__ == "__main__":
    # Initialize blinds
    blinds.read_blinds()

    # Create GUI
    root = tk.Tk()
    app = BlindsGUI(master=root)
    app.mainloop()
