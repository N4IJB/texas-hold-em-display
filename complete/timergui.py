import tkinter as tk
from poker_timer import Timer
import time

# Read the round time from round-time.txt
with open('round-time.txt', 'r') as f:
    round_time = int(f.read().strip())

timer = Timer(round_time)


def start_timer():
    timer.start()


def pause_timer():
    timer.pause()


def reset_timer():
    timer.reset()


def update_timer():
    time_left = timer.get_time_left()
    minutes = int(time_left // 60)
    seconds = int(time_left % 60)
    time_str = f"{minutes:02d}:{seconds:02d}"
    timer_label.config(text=time_str)
# round_time_remaining = timer.time_in_minutes - minutes - (seconds > 0)
    round_time_remaining_str = f"Round Time Remaining: "
    round_time_label.config(text=round_time_remaining_str)
    current_time = time.strftime("%H:%M:%S")
    current_time_label.config(text=f"Current Time: {current_time}")
    timer_label.after(100, update_timer)


# Create the GUI
root = tk.Tk()
root.title("Poker Timer")


current_time = time.strftime("%H:%M:%S")
current_time_label = tk.Label(root, font=('Helvetica', 12), text=f"Current Time: {current_time}")
current_time_label.pack()

round_time_label = tk.Label(root, font=('Helvetica', 16), text=f"Round Time Remaining: ")
round_time_label.pack()

timer_label = tk.Label(root, font=('Helvetica', 48), text=f"{round_time:02d}:00")
timer_label.pack()

start_button = tk.Button(root, text="Start", command=start_timer)
start_button.pack(side=tk.LEFT)

pause_button = tk.Button(root, text="Pause", command=pause_timer)
pause_button.pack(side=tk.LEFT)

reset_button = tk.Button(root, text="Reset", command=reset_timer)
reset_button.pack(side=tk.LEFT)

update_timer()

root.mainloop()
