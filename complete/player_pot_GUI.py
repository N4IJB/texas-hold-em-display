import tkinter as tk
from player_pot import Tournament

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.tournament = Tournament(int(open('buy-in.txt').read().strip()))
        self.create_widgets()

    def create_widgets(self):
        self.add_button = tk.Button(self, text="Add Player", command=self.add_player)
        self.add_button.pack(side="left")

        self.remove_button = tk.Button(self, text="Remove Player", command=self.remove_player)
        self.remove_button.pack(side="left")

        self.rebuy_button = tk.Button(self, text="Rebuy", command=self.rebuy)
        self.rebuy_button.pack(side="left")

        self.quit_button = tk.Button(self, text="Quit", command=self.master.quit)
        self.quit_button.pack(side="right")

        self.player_count_label = tk.Label(self, text="Players: " + str(self.tournament.player_count()))
        self.player_count_label.pack(side="top")

        self.pot_size_label = tk.Label(self, text="Pot Size: " + str(self.tournament.pot_size()))
        self.pot_size_label.pack(side="top")

        self.avg_chip_count_label = tk.Label(self, text="Average Chip Count: " + str(self.tournament.avg_chip_count()))
        self.avg_chip_count_label.pack(side="top")

    def add_player(self):
        self.tournament.add_player()
        self.update_labels()

    def remove_player(self):
        self.tournament.remove_player()
        self.update_labels()

    def rebuy(self):
        self.tournament.rebuy()
        self.update_labels()

    def update_labels(self):
        self.player_count_label.config(text="Players in tournament: " + str(self.tournament.player_count()))
        self.pot_size_label.config(text="Pot Size: $" + str(self.tournament.pot_size()))
        self.avg_chip_count_label.config(text="Average Chip Count: $" + str(self.tournament.avg_chip_count()))

root = tk.Tk()
app = Application(master=root)
app.pack()
app.mainloop()
