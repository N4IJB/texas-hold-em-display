import tkinter as tk
from poker_timer import Timer
import time
from player_pot import Tournament


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.tournament = Tournament(int(open('buy-in.txt').read().strip()))
        self.create_widgets()

    def create_widgets(self):
        self.add_button = tk.Button(self, text="Add Player", command=self.add_player)
        self.add_button.pack(side="right")

        self.remove_button = tk.Button(self, text="Remove Player", command=self.remove_player)
        self.remove_button.pack(side="right")

        self.rebuy_button = tk.Button(self, text="Rebuy", command=self.rebuy)
        self.rebuy_button.pack(side="right")

        self.quit_button = tk.Button(self, text="Quit", command=self.master.quit)
        self.quit_button.pack(side="right")

        self.player_count_label = tk.Label(self, text="Players: " + str(self.tournament.player_count()))
        self.player_count_label.pack(side="top")

        self.pot_size_label = tk.Label(self, text="Pot Size: " + str(self.tournament.pot_size()))
        self.pot_size_label.pack(side="top")

        self.avg_chip_count_label = tk.Label(self, text="Average Chip Count: " + str(self.tournament.avg_chip_count()))
        self.avg_chip_count_label.pack(side="top")

    def add_player(self):
        self.tournament.add_player()
        self.update_labels()

    def remove_player(self):
        self.tournament.remove_player()
        self.update_labels()

    def rebuy(self):
        self.tournament.rebuy()
        self.update_labels()

    def update_labels(self):
        self.player_count_label.config(text="Players in tournament: " + str(self.tournament.player_count()))
        self.pot_size_label.config(text="Pot Size: $" + str(self.tournament.pot_size()))
        self.avg_chip_count_label.config(text="Average Chip Count: $" + str(self.tournament.avg_chip_count()))


# Read the round time from round-time.txt
with open('round-time.txt', 'r') as f:
    round_time = int(f.read().strip())

timer = Timer(round_time)


def start_timer():
    timer.start()


def pause_timer():
    timer.pause()


def reset_timer():
    timer.reset()


def update_timer():
    time_left = timer.get_time_left()
    minutes = int(time_left // 60)
    seconds = int(time_left % 60)
    time_str = f"{minutes:02d}:{seconds:02d}"
    timer_label.config(text=time_str)
    round_time_remaining_str = f"Round Time Remaining: "
    round_time_label.config(text=round_time_remaining_str)
    current_time = time.strftime("%H:%M:%S")
    current_time_label.config(text=f"Current Time: {current_time}")
    timer_label.after(100, update_timer)


# Create the GUI
root = tk.Tk()
root.title("Poker Timer")

# create the timer section of the GUI
current_time = time.strftime("%H:%M:%S")
current_time_label = tk.Label(root, font=('Helvetica', 12), text=f"Current Time: {current_time}")
current_time_label.pack()

round_time_label = tk.Label(root, font=('Helvetica', 16), text=f"Round Time Remaining: ")
round_time_label.pack()

timer_label = tk.Label(root, font=('Helvetica', 48), text=f"{round_time:02d}:00")
timer_label.pack()

start_button = tk.Button(root, text="Start", command=start_timer)
start_button.pack(side=tk.LEFT)

pause_button = tk.Button(root, text="Pause", command=pause_timer)
pause_button.pack(side=tk.LEFT)

reset_button = tk.Button(root, text="Reset", command=reset_timer)
reset_button.pack(side=tk.LEFT)

update_timer()

app = Application(master=root)
app.pack(side="bottom")

root.mainloop()
