import time
import blinds

class Timer:
    def __init__(self, time_in_minutes):
        self.time_in_minutes = time_in_minutes
        self.seconds_left = time_in_minutes * 60
        self.start_time = time.time()
        self.is_running = False

    def start(self):
        self.is_running = True
        self.start_time = time.time()

    def pause(self):
        self.is_running = False
        self.seconds_left -= time.time() - self.start_time

    def reset(self):
        self.seconds_left = self.time_in_minutes * 60
        self.is_running = False

    def get_time_left(self):
        if self.is_running:
            time_left = self.seconds_left - (time.time() - self.start_time)
        else:
            time_left = self.seconds_left
        return max(0, time_left)
