import os

current_small_blind = 0
current_big_blind = 0
blinds_list = []
current_round_number = 1

def update_blinds():
    global current_small_blind, current_big_blind

    if blinds_list:
        small_blind, big_blind = blinds_list.pop(0)
    else:
        current_small_blind *= 2
        current_big_blind *= 2
        small_blind, big_blind = current_small_blind, current_big_blind

    current_small_blind, current_big_blind = int(small_blind), int(big_blind)
    return current_small_blind, current_big_blind

def read_blinds():
    global blinds_list, current_small_blind, current_big_blind

    with open('blinds.txt', 'r') as f:
        blinds_lines = f.readlines()

    blinds_list = []
    for line in blinds_lines:
        small_blind, big_blind = line.strip().split('/')
        blinds_list.append((int(small_blind), int(big_blind)))

    if not blinds_list:
        current_small_blind, current_big_blind = 25, 50
    else:
        current_small_blind, current_big_blind = blinds_list.pop(0)

    # reset the current round number to 1 when reading new blinds
    global current_round_number
    current_round_number = 1

read_blinds()

def write_blinds():
    global blinds_list

    with open('blinds.txt', 'w') as f:
        for small_blind, big_blind in blinds_list:
            f.write(f"{small_blind}/{big_blind}\n")

    if not blinds_list:
        with open('blinds.txt', 'a') as f:
            f.write(f"{current_small_blind}/{current_big_blind}\n")

def add_blinds(small_blind, big_blind):
    global blinds_list

    blinds_list.append((small_blind, big_blind))
    write_blinds()

def remove_blinds(index):
    global blinds_list

    del blinds_list[index]
    write_blinds()

def edit_blinds(index, small_blind, big_blind):
    global blinds_list

    blinds_list[index] = (small_blind, big_blind)
    write_blinds()

def update_round_number():
    global current_round_number
    current_round_number += 1

