class Tournament:
    def __init__(self, buyin):
        self.buyin = buyin
        self.players = []
        self.pot = 0

    def add_player(self):
        self.players.append(1)
        self.pot += self.buyin

    def remove_player(self):
        if len(self.players) > 0:
            self.players.pop()

    def rebuy(self):
        if len(self.players) > 0:
            self.pot += self.buyin

    def player_count(self):
        return len(self.players)

    def pot_size(self):
        return self.pot

    def avg_chip_count(self):
        if len(self.players) > 0:
            return round(self.pot / len(self.players), 2)
        else:
            return 0
