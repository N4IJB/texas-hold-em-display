import cv2
import numpy as np
import os
from pypokerengine.engine.card import Card
from pypokerengine.engine.deck import Deck
from pypokerengine.engine.hand_evaluator import HandEvaluator

# Define the card denomination and suit mapping
card_values = {
    "A": 14,
    "K": 13,
    "Q": 12,
    "J": 11,
    "10": 10,
    "9": 9,
    "8": 8,
    "7": 7,
    "6": 6,
    "5": 5,
    "4": 4,
    "3": 3,
    "2": 2,
}

card_suits = {
    "H": "hearts",
    "S": "spades",
    "D": "diamonds",
    "C": "clubs",
}

class Player:
    def __init__(self, name):
        self.name = name
        self.hand = []

    def add_card(self, card):
        self.hand.append(card)

    def calculate_hand_strength(self, community_cards):
        hole_cards = [Card.from_str(card) for card in self.hand]
        board_cards = [Card.from_str(card) for card in community_cards]

        deck = Deck()
        for card in hole_cards + board_cards:
            deck.remove_card(card)

        simulation_results = HandEvaluator.gen_hand_rank_info(
            hole_cards, board_cards, deck.draw_cards(5 - len(board_cards)), 1000
        )

        hand_strength = simulation_results["hand"]["strength"] / 100.0
        return hand_strength

class CommunityCards:
    def __init__(self):
        self.cards = []

    def add_card(self, card):
        self.cards.append(card)

# Initialize the player and community card objects
players = []
community_cards = CommunityCards()

# Load the card images
card_images = {}
for value in card_values:
    for suit in card_suits:
        card_image_path = os.path.join("cards", f"{value}{suit}.jpeg")
        card_images[f"{value}{suit}"] = cv2.imread(card_image_path)

# Initialize the GUI
cv2.namedWindow("Texas Hold'em", cv2.WINDOW_NORMAL)
cv2.resizeWindow("Texas Hold'em", 1600, 900)

# Function to update the display
def update_display():
    display_image = np.zeros((900, 1600, 3), dtype=np.uint8)

    # Display the community cards
    if len(community_cards.cards) >= 3:
        display_image[150:366, 220:360] = card_images[community_cards.cards[0]]
        display_image[150:366, 360:500] = card_images[community_cards.cards[1]]
        display_image[150:366, 500:640] = card_images[community_cards.cards[2]]

    if len(community_cards.cards) >= 4:
        display_image[150:366, 640:780] = card_images[community_cards.cards[3]]

    if len(community_cards.cards) >= 5:
        display_image[150:366, 780:920] = card_images[community_cards.cards[4]]

    # Display the players and their hands
    y_pos = 500
    for player in players:
        display_image[y_pos:y_pos + 216, 100:240] = card_images[player.hand[0]]
        display_image[y_pos:y_pos + 216, 240:380
