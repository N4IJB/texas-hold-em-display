import time


class PokerRoundClock:
    def __init__(self):
        self.start_time = 0
        self.pause_time = 0
        self.remaining_time = self.get_round_time() * 60
        self.round_number = 1
        self.small_blind, self.big_blind = self.get_blinds()

    def get_round_time(self):
        with open('round-time.txt', 'r') as file:
            return int(file.read().strip())

    def get_blinds(self):
        with open('blinds.txt', 'r') as file:
            lines = file.readlines()
            if len(lines) == 0:
                return 1, 2
            else:
                line = lines.pop(0).strip()
                if '/' not in line:
                    return 1, 2
                else:
                    small_blind, big_blind = map(int, line.split('/'))
                    return small_blind, big_blind

    def get_countdown_time(self):
        current_time = time.time()
        elapsed_time = current_time - self.start_time
        remaining_time = self.remaining_time - elapsed_time
        if remaining_time < 0:
            remaining_time = 0
            self.reset_countdown()
        return remaining_time

    def format_time(self, time_in_seconds):
        minutes, seconds = divmod(time_in_seconds, 60)
        return '{:02d}:{:02d}'.format(minutes, seconds)

    def start_countdown(self):
        self.start_time = time.time()
        if self.pause_time:
            self.remaining_time += (self.start_time - self.pause_time)
            self.pause_time = 0

    def pause_countdown(self):
        if self.start_time:
            self.pause_time = time.time()

    def reset_countdown(self):
        self.start_time = 0
        self.pause_time = 0
        self.remaining_time = self.get_round_time() * 60

    def is_countdown_running(self):
        return self.start_time and not self.pause_time

    def get_round_number(self):
        return self.round_number

    def get_small_blind(self):
        return self.small_blind

    def get_big_blind(self):
        return self.big_blind

    def increase_blinds(self):
        with open('blinds.txt', 'r') as file:
            lines = file.readlines()
            if len(lines) == 0:
                self.small_blind *= 2
                self.big_blind *= 2
            else:
                line = lines.pop(0).strip()
                if '/' not in line:
                    self.small_blind *= 2
                    self.big_blind *= 2
                else:
                    small_blind, big_blind = map(int, line.split('/'))
                    self.small_blind, self.big_blind = small_blind, big_blind
        with open('blinds.txt', 'w') as file:
            file.writelines(lines)

        self.round_number += 1
